__author__ = 'S. Konstantaras'
import json

import requests

import topoReader


class Communicator():
    def __init__(self, tr=topoReader.topoReader):
        self.ti_url = tr.get_ti().get('url')
        self.ls_url = tr.get_ls().get('url')
        self.tc_list = tr.get_all_tcs()
        self.tp_list = tr.get_all_tps()
        self.stp_list = {}

    def return_pf_url(self):
        for tc, val in self.tc_list.items():
            if "pathfinder" in val.get('type'):
                return val.get('url')
        return None

    def return_mn_url(self):
        for tc, val in self.tc_list.items():
            if "monitoring" in val.get('type'):
                return val.get('url')
        return None

    def get_ti_index(self):
        ti = self.ti_url
        r = requests.Response
        try:
            r = requests.get(ti)
        except:
            print("GUI: Failed to contact TI (%s)" % r.status_code)
            return None
            pass
        if r.status_code == 201:
            return json.loads(r.text)
        return None

    def get_ti_info(self):
        r = requests.Response
        try:
            r = requests.get(self.ti_url + "/info")
        except:
            print("GUI: Failed to contact TI (%s)" % r.status_code)
            pass
        if r.status_code == 201:
            return json.loads(r.text)
        return {}

    def get_ti_subs(self):
        r = requests.Response
        try:
            r = requests.get(self.ti_url + "/subscribers")
        except:
            print("GUI: Failed to contact TI (%s)" % r.status_code)
            pass
        if r.status_code == 201:
            return dict(json.loads(r.text))
        return {}

    def get_ti_updates_info(self):
        r = requests.Response
        try:
            r = requests.get(self.ti_url + "/updates_info")
        except:
            print("GUI: Failed to contact TI (%s)" % r.status_code)
            pass
        if r.status_code == 201:
            return dict(json.loads(r.text))
        return {}

    def get_ls_info(self):
        r = requests.Response
        try:
            r = requests.get(self.ls_url + "/info")
        except:
            print("GUI: Failed to contact LS (%s)" % r.status_code)
            pass
        if r.status_code == 201:
            return json.loads(r.text)
        return {}

    def get_all_stps(self):
        r = requests.Response
        try:
            r = requests.get(self.ls_url)
        except:
            print("GUI: Failed to contact LS (%s)" % r.status_code)
            pass
        if r.status_code == 200:
            self.stp_list = dict(json.loads(r.text)).get('stps')
        return self.stp_list

    def return_stp_list(self):
        # This can create loops. Should be fixed
        if self.stp_list.__len__() < 1:
            self.get_all_stps()
        new_list = []
        for key, value in self.stp_list.iteritems():
            new_list.append((key, value))
        return new_list

    def return_domain_list(self):
        # This can create loops. Should be fixed
        # NEEDS WORK FOR RETURNING VALUES!!!
        if self.stp_list.__len__() < 1:
            self.get_all_stps()
        new_list = []
        for key, value in self.stp_list.iteritems():
            new_list.append((value, ""))
        return new_list

    def get_stps_by_domain(self):
        # This can create loops. Should be fixed
        if self.stp_list.__len__() < 1:
            self.get_all_stps()
        newdict = {}
        for key, val in self.stp_list.iteritems():
            newdict.setdefault(val, []).append(key)
        return newdict

    def send_path_request(self, from_stp, to_stp, viad, notviad):
        url = "%s/pathfinder" % self.return_pf_url()
        path_request = json.dumps(
            {'request': {'from': from_stp, 'to': to_stp, 'via_domain': viad, 'not_domain': notviad}})
        # print json.dumps(path_request)
        headers = {'Content-Type': 'application/json'}
        try:
            r = requests.post(url, data=path_request, headers=headers)
            if r.status_code == 201:
                return json.loads(r.text)
        except:
            pass
        return {'Path': 'No_path_available'}

    def get_tc_info(self, tc_url):
        r = requests.Response
        try:
            r = requests.get(tc_url)
        except:
            print("GUI: Failed to contact TC (%s)" % r.status_code)
            return None
            pass
        if r.status_code == 201:
            return json.loads(r.text)
        return {}

    def get_tc_paths(self):
        tc = self.return_pf_url() + "/paths"
        r = requests.Response
        try:
            r = requests.get(tc)
        except:
            print("GUI: Failed to contact TC (%s)" % r.status_code)
            return None
            pass
        if r.status_code == 201:
            return json.loads(r.text)
        return {}

    def get_tc_graph(self):
        tc = self.return_mn_url() + "/getgraph"
        r = requests.Response
        try:
            r = requests.get(tc)
        except:
            print("GUI: Failed to contact TC (%s)" % r.status_code)
            return None
            pass
        if r.status_code == 201:
            return json.loads(r.text)
        return {}

    def get_tp_info(self):
        all_tp_info = {}
        for tp, values in self.tp_list.items():
            r = requests.Response
            try:
                r = requests.get(values.get('url'))
            except:
                print("GUI: Failed to contact TP %s (%s)" % (values.get('name'), r.status_code))
                pass
            if r.status_code == 201:
                all_tp_info[values.get('name')] = dict(json.loads(r.text))
        return all_tp_info