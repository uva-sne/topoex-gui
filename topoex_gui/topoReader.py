__author__ = 'stavros'
import io
import json


class topoReader():
    def __init__(self, location):
        data = io.open(location)
        self.json_data = json.load(data, encoding='utf-8')
        # io.close(location)

    def get_ti(self):
        return self.json_data.get('ti')

    def get_ls(self):
        return self.json_data.get('ls')

    def get_tc(self):
        return self.json_data.get('tc')

    def get_all_tps(self):
        tps = {}
        for k in self.json_data.keys():
            if k.startswith('tp'):
                tps[k] = self.json_data.get(k)
        return dict(tps)

    def get_all_tcs(self):
        tcs = {}
        for k in self.json_data.keys():
            if k.startswith('tc'):
                tcs[k] = self.json_data.get(k)
        return dict(tcs)
