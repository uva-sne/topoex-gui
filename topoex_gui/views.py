__author__ = 'S. Konstantaras'
import os
import json

from django import forms
from django.http import HttpResponse
from django.template import loader, RequestContext

from topoexCommunicator import Communicator as CM
from topoReader import topoReader

#tpr = topoReader("/home/stavros/topoex_logs/overview.json")
tpr = topoReader(os.getcwd().__str__() + "/overview.json")
com_obj = CM(tpr)


class stpForm(forms.Form):
    data = com_obj.return_stp_list()
    domains = com_obj.return_domain_list()
    from_stp = forms.ChoiceField(label='from_stp', required=True, choices=data)
    to_stp = forms.ChoiceField(label='to_stp', required=True, choices=data)
    via_domain = forms.ChoiceField(label='via_domain', required=False, choices=domains)
    not_domain = forms.ChoiceField(label='not_domain', required=False, choices=domains)


def sort_by_key(dict):
    new_dict = {}
    for key in sorted(dict.iterkeys()):
        new_dict[key] = dict[key]
    return new_dict


def get_pf_context(request, path=None, from_stp=None, to_stp=None):
    stp_dict = com_obj.get_stps_by_domain()
    if stp_dict is not None:
        if path is not None:
            context = RequestContext(request,
                                     {'title': 'Pathfinder', 'json_data': json.dumps(stp_dict), 'stps': stp_dict,
                                      'new_path': path, 'from_stp': from_stp, 'to_stp': to_stp})
        else:
            context = RequestContext(request,
                                     {'title': 'Pathfinder', 'json_data': json.dumps(stp_dict), 'stps': stp_dict,
                                      'new_path': "", 'from_stp': "", 'to_stp': ""})
    else:
        context = RequestContext(request, {'title': 'Pathfinder', 'new_path': ""})
    return context


def home(request):
    temp = os.path.join(os.path.dirname(__file__), "../templates/index.html")
    template = loader.get_template(temp)
    context = RequestContext(request, {'title': 'Topology Exchange demo'})
    return HttpResponse(template.render(context))


def show_ti(request):
    temp = os.path.join(os.path.dirname(__file__), "../templates/topology_index.html")
    template = loader.get_template(temp)
    index = com_obj.get_ti_index()
    if index is not None:
        context = RequestContext(request, {'title': 'Topology Index Overview', 'domains': index})
    else:
        context = RequestContext(request, {'title': 'Topology Index Overview'})
    return HttpResponse(template.render(context))


def show_stps(request):
    temp = os.path.join(os.path.dirname(__file__), "../templates/lookup_service.html")
    template = loader.get_template(temp)
    context = RequestContext(request, {'title': 'Topology Index Overview', 'stps': com_obj.get_stps_by_domain()})
    return HttpResponse(template.render(context))


def show_paths(request):
    temp = os.path.join(os.path.dirname(__file__), "../templates/paths.html")
    template = loader.get_template(temp)
    context = RequestContext(request, {'title': 'Pathfinding History', 'paths': sort_by_key(com_obj.get_tc_paths())})
    return HttpResponse(template.render(context))


def show_pf(request):
    temp = os.path.join(os.path.dirname(__file__), "../templates/pathfinder.html")
    template = loader.get_template(temp)
    return HttpResponse(template.render(get_pf_context(request, None)))


def path_submit(request):
    temp = os.path.join(os.path.dirname(__file__), "../templates/pathfinder.html")
    template = loader.get_template(temp)
    if request.method == 'POST':
        form = stpForm(request.POST)
        if form.is_valid():
            path = com_obj.send_path_request(form.cleaned_data['from_stp'], form.cleaned_data['to_stp'],
                                             form.cleaned_data['via_domain'], form.cleaned_data['not_domain'])
            # print path
            return HttpResponse(template.render(get_pf_context(request, path, form.cleaned_data['from_stp'],
                                                               form.cleaned_data['to_stp'])))
        else:
            print "Form is not valid %s" % form.errors
            return HttpResponse(template.render(get_pf_context(request, None)))


def overview(request):
    ti_info = com_obj.get_ti_info()
    tc_pf_info = com_obj.get_tc_info(com_obj.return_pf_url())
    ls_info = com_obj.get_ls_info()
    temp = os.path.join(os.path.dirname(__file__), "../templates/overview.html")
    template = loader.get_template(temp)
    context = RequestContext(request, {'title': 'Topology Overview', 'ti_url': ti_info.get('url'),
                                       'ti_size': ti_info.get('index_size'), 'ti_subs': ti_info.get('sub_size'),
                                       'ti_updates': ti_info.get('updates'), 'tc_url': tc_pf_info.get('url'),
                                       'tc_name': tc_pf_info.get('name'), 'tc_type': tc_pf_info.get('type'),
                                       'tc_domains': tc_pf_info.get('domains'), 'tc_updates': tc_pf_info.get('updates'),
                                       'tc_time_limit': tc_pf_info.get('time_limit'),
                                       'tc_paths_number': tc_pf_info.get('paths'),
                                       'ls_url': ls_info.get('url'), 'ls_size': ls_info.get('stps'), 'ls_interval':
        ls_info.get('upd_interval'), 'tps': com_obj.get_tp_info()})
    return HttpResponse(template.render(context))


def show_ti_subs(request):
    ti_subs = com_obj.get_ti_subs()
    temp = os.path.join(os.path.dirname(__file__), "../templates/ti_subs.html")
    template = loader.get_template(temp)
    context = RequestContext(request, {'title': 'Topology Index subscribers', 'ti_subs': ti_subs})
    return HttpResponse(template.render(context))


def show_ti_updates_info(request):
    ti_updates = com_obj.get_ti_updates_info()
    temp = os.path.join(os.path.dirname(__file__), "../templates/ti_updates_info.html")
    template = loader.get_template(temp)
    context = RequestContext(request, {'title': 'Topology Index subscribers', 'ti_updates': ti_updates})
    return HttpResponse(template.render(context))


def show_graph(request):
    temp = os.path.join(os.path.dirname(__file__), "../templates/graph.html")
    template = loader.get_template(temp)
    json_graph = com_obj.get_tc_graph()
    context = RequestContext(request, {'title': "Network Graph", 'timestamp': json_graph.get('timestamp'),
                                       'svg_width': 960, 'svg_height': 960,
                                       'json_data': json.dumps(json_graph.get('netgraph'))})
    return HttpResponse(template.render(context))


def show_details(request):
    temp = os.path.join(os.path.dirname(__file__), "../templates/details.html")
    template = loader.get_template(temp)
    context = RequestContext(request, {'title': "Architecture details"})
    return HttpResponse(template.render(context))
