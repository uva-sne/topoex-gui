from django.conf.urls import patterns, include, url
from django.contrib import admin

# SITE_MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'topoex_gui/', 'static/')
urlpatterns = patterns('',
                       url(r'^$', 'topoex_gui.views.home', name='home'),
                       url(r'^ti$', 'topoex_gui.views.show_ti', name='show_ti'),
                       url(r'^show_stps$', 'topoex_gui.views.show_stps', name='show_stps'),
                       url(r'^pf$', 'topoex_gui.views.show_pf', name='show_pf'),
                       url(r'^path_submit$', 'topoex_gui.views.path_submit', name='path_submit'),
                       url(r'^graph$', 'topoex_gui.views.show_graph', name='show_graph'),
                       url(r'^paths$', 'topoex_gui.views.show_paths', name='show_paths'),
                       url(r'^overview$', 'topoex_gui.views.overview', name='overview'),
                       url(r'^details$', 'topoex_gui.views.show_details', name='show_details'),
                       url(r'^ti_subs$', 'topoex_gui.views.show_ti_subs', name='ti_subs'),
                       url(r'^ti_updates_info$', 'topoex_gui.views.show_ti_updates_info', name='ti_updates_info'),
                       # url(r'^blog/', include('blog.urls')),
                       url(r'^admin/', include(admin.site.urls)),
)
